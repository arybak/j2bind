# j2bind

Plugin version 0.0.1.   

jQuery plugin for two way data binding.

## Usage

Add script ```src/j2bind.js``` to your page, after jQuery script.  

Write in your own script:  

``` $('selector').j2bind(params); ```  
``` params  ``` is object.  
```
foo = {
  bar: "ololo";
}

params = {
  obj: foo, //object for two way binding
  prop: "bar" //string name of properties of object for two way binding 
}
```

You can see the demos on exapmle/demo.html.