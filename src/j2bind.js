/**
*  jQuery 2 way binding plugin
*  autor: Alex Rybak
*  alexrybak.pr@gmail.com
*  @AlexRybakUA
*/
(function ($) {
    /**
    * private j2bind method plugin functions
    */
    var _methods = {
        /**
        * define dom element content change function name
        * returns string name of function
        * params @type - string with dom element tag name
        */
        defineDomObjFunc : function (type) {

            if(type === 'a' || type === 'span' || type === 'div' 
                || type === 'td' || type === 'th' || type === 'li') {
                return "html";
            } else if (type === 'input') {
                return "val";
            }
        },
        getNextId: function () {
            var id = -1;

            return function () {
                return id++;
            }();
        },
        /**
        * override Object get and set methods
        * params 
        * @obj - object value of what will 2 way bind
        * @prop - string name of @obj propertie that will be updating 
        * @$domElem - jQuery reference to DOM element value or content of what - will be 2 way bind
        * @func - string with @$domElem content access function('value' or 'html')  
        */
        bind2: function (obj,  prop, $domElem, func) {
            var id;

            if(!obj.j2bindId) {
                obj.j2bindId = _methods.getNextId();
            }
            id = obj.j2bindId;
            options.objDoms[id] = options.objDoms[id] || [];
            options.objDoms[id].push($domElem);

            Object.defineProperty(obj, prop, {
                get: function() { 
                    // return $domElem[func]();
                    if(options.objDoms[id].length) { //if we set value befotr get
                        return options.objDoms[id][0][func](); //return fist element of DOM arrays
                    }                               //(all DOM elements for ome object has the same value)

                    return undefined;
                }, 
                set: function(newValue) {
                    // $domElem[func](newValue);
                    $.each(options.objDoms[id], function (key, value) {
                        value[func](newValue);
                    });
                },
                configurable: true
            });
        }
    },
    /**
    * public j2bind method plugin functions
    */
    methods = {
        init: function (userOptions) {
            var opt;

            this.options = $.extend(this.options, userOptions);
            opt = this.options;

            return this.each(function () {
                var $this = $(this) ,
                    func = _methods.defineDomObjFunc($this[0].localName);
                    
                    _methods.bind2(opt.obj, opt.prop, $this, func);                 
            });
        }   
    }, 
    /**
    * default plugin options
    */
    options = {
        obj: undefined,
        prop: undefined,
        objDoms: {}
    };

    $.fn.j2bind = function (method) {

        if( methods[method] ) { //if passed name of method
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1)); //call public method
        } else if ( typeof method === 'object' || !method ) { //if passed objec with options
                return methods.init.apply(this, arguments); //call init function with this options
        } else {
            $.error("Method with name: " + method + " doesnt exists for j2bind pligin!");
        }
    }
    
})(jQuery);
